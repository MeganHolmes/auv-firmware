# AUV Firmware

## Description
Firmware for capstone

## Installation
Clone this repo then run tools/install-zephyr.sh
Use tools/build-and-run.sh during development

## Usage
TODO

## Authors and acknowledgment
Megan Holmes    - Software
Morgan Buck     - Electrical
Zoey Van Rassel - Mechanical
Brock Myers     - Mechanical

## License
TODO
