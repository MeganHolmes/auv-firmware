// This file is responsible for the translation of desired states into actions
// and executes those actions.

// System includes
#include <stdlib.h>
#include <assert.h>
#include <zephyr.h>
#include <logging/log.h>

// Project Includes
#include "config.h"
#include "control.h"
#include "state.h"

// Functions constrained to this .c file

// Setup logging
LOG_MODULE_REGISTER(CONTROL, AUV_LOG_LEVEL);

/*******************************************************************************
 * @brief Entry point for control thread
 *
 * @param thread_info Pointer to thread info struct
 * @param mem_ptrs    Pointers to the different states control needs to track
 ******************************************************************************/
void control_main(struct Control_thread_info* thread_info,
    struct Control_state_memory_pointers* mem_ptrs)
{
    LOG_INF("Control thread started");

    struct AUV_state* current_state  = mem_ptrs->current_state;
    struct AUV_state* desired_state  = mem_ptrs->desired_state;
    struct Auv_state* previous_state = mem_ptrs->previous_state;

    struct Commands*  actions = malloc(sizeof(struct Commands));

    while (true)
    {
        // Get the states from the state and navigation threads

        // Translate the desired state into commands
        actions = translate_desired_state_to_commands(
            current_state,
            desired_state);

        // Execute the commands
        bool success = execute_commands(actions);

        // Output the commands
        output_commands(actions, LOG_LEVEL_DBG);

        thread_info->last_run_time = k_uptime_get();

        LOG_WRN("Control thread not implemented yet!");
        #ifdef DEVELOPMENT_MODE
        k_sleep(K_SECONDS(DEVELOPMENT_MODE_SLEEP_TIME));
        #endif
        k_yield();
    }

    LOG_ERR("Control thread exiting!");
}

/*******************************************************************************
 * @brief Translates a desired state into commands to achieve that state
 *
 * @param current_state The current state of the system
 * @param desired_state The desired state of the system
 * @return Commands to achieve the desired state
 ******************************************************************************/
struct Commands* translate_desired_state_to_commands(
    struct Auv_state* current_state,
    struct Auv_state* desired_state)
{
    assert(current_state != NULL);
    assert(desired_state != NULL);

    LOG_WRN("Method translate_desired_state_to_commands not yet implemented!");

    struct Commands* actions = malloc(sizeof(struct Commands));

    actions->left_aileron  = 0;
    actions->right_aileron = 0;
    actions->elevator      = 0;
    actions->rudder        = 0;
    actions->throttle      = 0;

    return actions;
}

/*******************************************************************************
 * @brief Executes the desired commands by sending them to the actuators
 *
 * @param actions The desired actions to execute
 *
 * @return True if successful, false otherwise
 ******************************************************************************/
bool execute_commands(struct Commands* actions)
{
    assert(actions != NULL);

    LOG_WRN("Method execute_commands not yet implemented!");

    return false;
}

/*******************************************************************************
 * @brief Outputs the commands to the log
 *
 * @param actions The commands to output
 * @param log_level The log level to output to
 ******************************************************************************/
void output_commands(struct Commands* actions,
                     unsigned int     log_level)
{
    assert(actions != NULL);

    switch (log_level)
    {
    case LOG_LEVEL_ERR:
    {
        LOG_ERR("--- Commands ---");
        LOG_ERR("Left Aileron:  %f rad", actions->left_aileron);
        LOG_ERR("Right Aileron: %f rad", actions->right_aileron);
        LOG_ERR("Elevator:      %f rad", actions->elevator);
        LOG_ERR("Rudder:        %f rad", actions->rudder);
        LOG_ERR("Throttle:      %f percent", actions->throttle);
        break;
    }
    case LOG_LEVEL_WRN:
    {
        LOG_WRN("--- Commands ---");
        LOG_WRN("Left Aileron:  %f rad", actions->left_aileron);
        LOG_WRN("Right Aileron: %f rad", actions->right_aileron);
        LOG_WRN("Elevator:      %f rad", actions->elevator);
        LOG_WRN("Rudder:        %f rad", actions->rudder);
        LOG_WRN("Throttle:      %f percent", actions->throttle);
        break;
    }
    case LOG_LEVEL_INF:
    {
        LOG_INF("--- Commands ---");
        LOG_INF("Left Aileron:  %f rad", actions->left_aileron);
        LOG_INF("Right Aileron: %f rad", actions->right_aileron);
        LOG_INF("Elevator:      %f rad", actions->elevator);
        LOG_INF("Rudder:        %f rad", actions->rudder);
        LOG_INF("Throttle:      %f percent", actions->throttle);
        break;
    }
    case LOG_LEVEL_DBG:
    {
        LOG_DBG("--- Commands ---");
        LOG_DBG("Left Aileron:  %f rad", actions->left_aileron);
        LOG_DBG("Right Aileron: %f rad", actions->right_aileron);
        LOG_DBG("Elevator:      %f rad", actions->elevator);
        LOG_DBG("Rudder:        %f rad", actions->rudder);
        LOG_DBG("Throttle:      %f percent", actions->throttle);
        break;
    }
    default:
    {
        LOG_ERR("Invalid log_level used in output_commands!");
    }
    }
}
