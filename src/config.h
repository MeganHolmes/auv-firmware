// Contains all configuration options and toggles

#pragma once

// System Includes
#include <logging/log.h>

// Project Includes
#include "constants.h"

// Anything below the threshold is written to a log.
// Options:
// LOG_LEVEL_NONE
// LOG_LEVEL_ERR
// LOG_LEVEL_WRN
// LOG_LEVEL_INF  (suggested default)
// LOG_LEVEL_DBG
#define AUV_LOG_LEVEL LOG_LEVEL_INF

// If this is true adds a k_sleep() to all loops. Set the sleep time in
// DEVELOPMENT_MODE_SLEEP_TIME in constants.h
#define DEVELOPMENT_MODE true
