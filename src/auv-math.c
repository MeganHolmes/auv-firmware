// Implementation of general math and physics functions

// System Includes
#include <math.h>
#include <logging/log.h>

// Project Includes
#include "auv-math.h"
#include "config.h"

// Setup logging
LOG_MODULE_REGISTER(MATH, AUV_LOG_LEVEL);

/*******************************************************************************
 * @brief Discrete-time implementation of a simple RC low-pass filter
 *
 * https://en.wikipedia.org/wiki/Low-pass_filter#Discrete-time_realization
 *
 * @param[in] x_old       Old value
 * @param[in] x_new       New value
 * @param[in] cutoff_freq Cutoff frequency
 * @param[in] dt          Time step
 * @return Filtered value
 ******************************************************************************/
float low_pass_filter(float x_old,
                      float x_new,
                      float cutoff_freq,
                      float dt)
{
    float alpha = 2.0f * M_PI * cutoff_freq * dt /
                 (2.0f * M_PI * cutoff_freq * dt + 1.0f);

    return (x_old * alpha) + (x_new * (1 - alpha));
}

/*******************************************************************************
 * @brief Calculate straight-line distance between two points in 3D
 *
 * @param[in] x1 X coordinate of first point
 * @param[in] y1 Y coordinate of first point
 * @param[in] z1 Z coordinate of first point
 * @param[in] x2 X coordinate of second point
 * @param[in] y2 Y coordinate of second point
 * @param[in] z2 Z coordinate of second point
 *
 * @return Distance between points
 ******************************************************************************/
float calculate_3d_distance(float x1,
                            float y1,
                            float z1,
                            float x2,
                            float y2,
                            float z2)
{
    return sqrtf(powf(x2 - x1, 2) + powf(y2 - y1, 2) + powf(z2 - z1, 2));
}

/*******************************************************************************
 * @brief Calculate vector length of a 3D vector
 *
 * @param[in] x X coordinate of vector
 * @param[in] y Y coordinate of vector
 * @param[in] z Z coordinate of vector
 *
 * @return Length of vector
 ******************************************************************************/
float calculate_3d_vector_length(float x,
                                 float y,
                                 float z)
{
    return sqrtf(powf(x, 2) + powf(y, 2) + powf(z, 2));
}

/*******************************************************************************
 * @brief Calculate the relative change between two values
 *
 * @param[in] x1 Old value
 * @param[in] x2 New value
 *
 * @return Relative change between x1 and x2
 ******************************************************************************/
int calculate_percent_change(float x_old,
                             float x_new)
{
    float delta = fabsf(x_new - x_old);
    float percent_change = delta / fmaxf(x_old, x_new); // Avoid divide by zero

    return (int)(percent_change * 100);
}
