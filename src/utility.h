// Methods to interact with sensors.

#pragma once

// System Includes

// Project Includes

// Defines

// Structure Definitions

// Enumeration Definitions

// Function Declarations

// Push the memory defined by a pointer onto a message queue
void push_to_msg_queue(struct k_msgq* msgq, char* msg)
{
    while (k_msgq_put(msgq, msg, K_NO_WAIT) != 0)
    {
        k_msgq_purge(msgq);
    };
};
