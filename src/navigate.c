// This file is responsible for determining the desired state of the system

// System includes
#include <stdlib.h>
#include <assert.h>
#include <zephyr.h>
#include <logging/log.h>

// Project Includes
#include "config.h"
#include "state.h"

// Functions constrained to this .c file

// Setup logging
LOG_MODULE_REGISTER(NAVIGATE, AUV_LOG_LEVEL);

/*******************************************************************************
 * @brief Entry point for navigate thread
 ******************************************************************************/
void navigate_main(void*, void*, void*)
{
    LOG_INF("Navigate thread started");

    struct Auv_state* current_state = NULL;
    struct Auv_state* desired_state = NULL;

    while(true)
    {
        // Get the states from the current state from the state thread

        int last_run_time = k_uptime_get(); // store this value somewhere so
                                            // main() can access it

        LOG_WRN("Navigate thread not implemented yet!");
        k_yield();
    }

    LOG_ERR("Navigate thread exiting!");
}

/*******************************************************************************
 * @brief Returns a state that results in increasing distance from obstacle as
 * quickly as possible
 *
 * @param[in] current_state The current state of the system
 * @param[in] data          Sensor data containing ultrasound information
 * @return A state to to increase distance from obstacle as quickly as possible
 ******************************************************************************/
struct Auv_state* avoid_collision(struct Auv_state*   current_state,
                                  struct Sensor_data* data)
{
    assert(current_state != NULL);
    assert(data != NULL);

    LOG_WRN("Method avoid_collision not yet implemented!");

    struct Auv_state* desired_state = init_auv_state();

    return desired_state;
}
