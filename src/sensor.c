// Methods to interact with sensors.

// System includes
#include <stdlib.h>
#include <assert.h>
#include <zephyr.h>
#include <logging/log.h>

// Project Includes
#include "constants.h"
#include "config.h"
#include "utility.h"
#include "sensor.h"

// Functions constrained to this .c file


// Setup logging
LOG_MODULE_REGISTER(SENSOR, AUV_LOG_LEVEL);

// Setup mutexs
extern struct k_mutex sensor_data_mutex;

/*******************************************************************************
 * @brief Entry point for sensor thread
 *
 ******************************************************************************/
void sensor_main(struct Sensor_thread_info* thread_info,
        struct Sensor_data* data, void*)
{
    LOG_INF("Sensor thread started");

    while (true)
    {
        retrieve_sensor_data(data);

        output_sensor_data(data, LOG_LEVEL_DBG);

        if (check_for_possible_collision(data))
        {
            LOG_WRN("Possible collision detected!");
            // TODO: set sensor, state, and control priorities to 0
        }
        else
        {
            LOG_DBG("No collision detected");
        }

        update_sensor_thread_info(thread_info, data);

        LOG_WRN("Sensor thread not implemented yet!");
        #ifdef DEVELOPMENT_MODE
        k_sleep(K_SECONDS(DEVELOPMENT_MODE_SLEEP_TIME));
        #endif
        k_yield();
    }

    LOG_ERR("Sensor thread exiting!");

    free(data);
    free(thread_info);
}

/*******************************************************************************
 * @brief Accesses the raw sensor data and converts it to a usable format
 *
 * @param data - pointer to a Sensor_data struct
 ******************************************************************************/
void retrieve_sensor_data(struct Sensor_data* data)
{
    LOG_WRN("Method retrieve_sensor_data not yet implemented!");

    // Temporary arbitrary values.
    k_mutex_lock(&sensor_data_mutex, K_FOREVER);
    data->forward_ultrasound_left = 1;
    data->forward_ultrasound_right = 1;
    data->left_ultrasound = 1;
    data->right_ultrasound = 1;
    data->bottom_ultrasound = 1;
    data->reverse_ultrasound = 1;
    data->gyro_roll = 0;
    data->gyro_pitch = 0;
    data->gyro_yaw = 0;
    data->accel_x = 0;
    data->accel_y = 0;
    data->accel_z = 0;
    data->mag_x = 0;
    data->mag_y = 0;
    data->mag_z = 0;
    data->static_pressure = 1;
    data->dynamic_pressure = 0;
    data->temperature = 20;
    k_mutex_unlock(&sensor_data_mutex);
}

/*******************************************************************************
 * @brief Checks if any of the sensors are within the minimum clearance
 *
 * @param[in] data Sensor data containing ultrasound information
 * @return True if any ultrasound is within minimum clearance, false otherwise
 ******************************************************************************/
bool check_for_possible_collision(struct Sensor_data *data)
{
    assert(data != NULL);

    bool result = false;

    if (data->forward_ultrasound_left < MINIMUM_OBSTACLE_CLEARANCE)
    {
        LOG_DBG("Forward left ultrasound reports distance less than minimum clearance!");
        result = true;
    }
    else if (data->forward_ultrasound_right < MINIMUM_OBSTACLE_CLEARANCE)
    {
        LOG_DBG("Forward right ultrasound reports distance less than minimum clearance!");
        result = true;
    }
    else if (data->left_ultrasound < MINIMUM_OBSTACLE_CLEARANCE)
    {
        LOG_DBG("Left ultrasound reports distance less than minimum clearance!");
        result = true;
    }
    else if (data->right_ultrasound < MINIMUM_OBSTACLE_CLEARANCE)
    {
        LOG_DBG("Right ultrasound reports distance less than minimum clearance!");
        result = true;
    }
    else if (data->bottom_ultrasound < MINIMUM_OBSTACLE_CLEARANCE)
    {
        LOG_DBG("Bottom ultrasound reports distance less than minimum clearance!");
        result = true;
    }
    else if (data->reverse_ultrasound < MINIMUM_OBSTACLE_CLEARANCE)
    {
        LOG_DBG("Reverse ultrasound reports distance less than minimum clearance!");
        result = true;
    }
    else
    {
        LOG_DBG("No ultrasound reports distance less than minimum clearance!");
    }

    return result;
}

/*******************************************************************************
 * @brief Outputs sensor data to log
 *
 * @param[in] data Sensor data to be output
 * @param[in] log_level Log level to output sensor data to
 ******************************************************************************/
void output_sensor_data(struct Sensor_data *data,
                        unsigned int        log_level)
{
    assert(data != NULL);

    switch (log_level)
    {
    case LOG_LEVEL_ERR:
    {
        LOG_ERR("--- Sensor Data ---");
        LOG_ERR("Forward Ultrasound Left:  %f m",
                data->forward_ultrasound_left);
        LOG_ERR("Forward Ultrasound Right: %f m",
                data->forward_ultrasound_right);
        LOG_ERR("Left Ultrasound:          %f m",
                data->left_ultrasound);
        LOG_ERR("Right Ultrasound:         %f m",
                data->right_ultrasound);
        LOG_ERR("Bottom Ultrasound:        %f m",
                data->bottom_ultrasound);
        LOG_ERR("Reverse Ultrasound:       %f m",
                data->reverse_ultrasound);
        LOG_ERR("Gyro X:                   %f rad/s",
                data->gyro_roll);
        LOG_ERR("Gyro Y:                   %f rad/s",
                data->gyro_pitch);
        LOG_ERR("Gyro Z:                   %f rad/s",
                data->gyro_yaw);
        LOG_ERR("Acceleration X:           %f m/s^2",
                data->accel_x);
        LOG_ERR("Acceleration Y:           %f m/s^2",
                data->accel_y);
        LOG_ERR("Acceleration Z:           %f m/s^2",
                data->accel_z);
        LOG_ERR("Magnetic Flux X:          %f Mx",
                data->mag_x);
        LOG_ERR("Magnetic Flux Y:          %f Mx",
                data->mag_y);
        LOG_ERR("Magnetic Flux Z:          %f Mx",
                data->mag_z);
        LOG_ERR("Static Pressure:          %f Pa",
                data->static_pressure);
        LOG_ERR("Dynamic Pressure:         %f Pa",
                data->dynamic_pressure);
        LOG_ERR("Temperature:              %f C",
                data->temperature);

        break;
    }
    case LOG_LEVEL_WRN:
    {
        LOG_WRN("--- Sensor Data ---");
        LOG_WRN("Forward Ultrasound Left:  %f m",
                data->forward_ultrasound_left);
        LOG_WRN("Forward Ultrasound Right: %f m",
                data->forward_ultrasound_right);
        LOG_WRN("Left Ultrasound:          %f m",
                data->left_ultrasound);
        LOG_WRN("Right Ultrasound:         %f m",
                data->right_ultrasound);
        LOG_WRN("Bottom Ultrasound:        %f m",
                data->bottom_ultrasound);
        LOG_WRN("Reverse Ultrasound:       %f m",
                data->reverse_ultrasound);
        LOG_WRN("Gyro X:                   %f rad/s",
                data->gyro_roll);
        LOG_WRN("Gyro Y:                   %f rad/s",
                data->gyro_pitch);
        LOG_WRN("Gyro Z:                   %f rad/s",
                data->gyro_yaw);
        LOG_WRN("Acceleration X:           %f m/s^2",
                data->accel_x);
        LOG_WRN("Acceleration Y:           %f m/s^2",
                data->accel_y);
        LOG_WRN("Acceleration Z:           %f m/s^2",
                data->accel_z);
        LOG_WRN("Magnetic Flux X:          %f Mx",
                data->mag_x);
        LOG_WRN("Magnetic Flux Y:          %f Mx",
                data->mag_y);
        LOG_WRN("Magnetic Flux Z:          %f Mx",
                data->mag_z);
        LOG_WRN("Static Pressure:          %f Pa",
                data->static_pressure);
        LOG_WRN("Dynamic Pressure:         %f Pa",
                data->dynamic_pressure);
        LOG_WRN("Temperature:              %f C",
                data->temperature);

        break;
    }
    case LOG_LEVEL_INF:
    {
        LOG_INF("--- Sensor Data ---");
        LOG_INF("Forward Ultrasound Left:  %f m",
                data->forward_ultrasound_left);
        LOG_INF("Forward Ultrasound Right: %f m",
                data->forward_ultrasound_right);
        LOG_INF("Left Ultrasound:          %f m",
                data->left_ultrasound);
        LOG_INF("Right Ultrasound:         %f m",
                data->right_ultrasound);
        LOG_INF("Bottom Ultrasound:        %f m",
                data->bottom_ultrasound);
        LOG_INF("Reverse Ultrasound:       %f m",
                data->reverse_ultrasound);
        LOG_INF("Gyro X:                   %f rad/s",
                data->gyro_roll);
        LOG_INF("Gyro Y:                   %f rad/s",
                data->gyro_pitch);
        LOG_INF("Gyro Z:                   %f rad/s",
                data->gyro_yaw);
        LOG_INF("Acceleration X:           %f m/s^2",
                data->accel_x);
        LOG_INF("Acceleration Y:           %f m/s^2",
                data->accel_y);
        LOG_INF("Acceleration Z:           %f m/s^2",
                data->accel_z);
        LOG_INF("Magnetic Flux X:          %f Mx",
                data->mag_x);
        LOG_INF("Magnetic Flux Y:          %f Mx",
                data->mag_y);
        LOG_INF("Magnetic Flux Z:          %f Mx",
                data->mag_z);
        LOG_INF("Static Pressure:          %f Pa",
                data->static_pressure);
        LOG_INF("Dynamic Pressure:         %f Pa",
                data->dynamic_pressure);
        LOG_INF("Temperature:              %f C",
                data->temperature);

        break;
    }
    case LOG_LEVEL_DBG:
    {
        LOG_DBG("--- Sensor Data ---");
        LOG_DBG("Forward Ultrasound Left:  %f m",
                data->forward_ultrasound_left);
        LOG_DBG("Forward Ultrasound Right: %f m",
                data->forward_ultrasound_right);
        LOG_DBG("Left Ultrasound:          %f m",
                data->left_ultrasound);
        LOG_DBG("Right Ultrasound:         %f m",
                data->right_ultrasound);
        LOG_DBG("Bottom Ultrasound:        %f m",
                data->bottom_ultrasound);
        LOG_DBG("Reverse Ultrasound:       %f m",
                data->reverse_ultrasound);
        LOG_DBG("Gyro X:                   %f rad/s",
                data->gyro_roll);
        LOG_DBG("Gyro Y:                   %f rad/s",
                data->gyro_pitch);
        LOG_DBG("Gyro Z:                   %f rad/s",
                data->gyro_yaw);
        LOG_DBG("Acceleration X:           %f m/s^2",
                data->accel_x);
        LOG_DBG("Acceleration Y:           %f m/s^2",
                data->accel_y);
        LOG_DBG("Acceleration Z:           %f m/s^2",
                data->accel_z);
        LOG_DBG("Magnetic Flux X:          %f Mx",
                data->mag_x);
        LOG_DBG("Magnetic Flux Y:          %f Mx",
                data->mag_y);
        LOG_DBG("Magnetic Flux Z:          %f Mx",
                data->mag_z);
        LOG_DBG("Static Pressure:          %f Pa",
                data->static_pressure);
        LOG_DBG("Dynamic Pressure:         %f Pa",
                data->dynamic_pressure);
        LOG_DBG("Temperature:              %f C",
                data->temperature);

        break;
    }
    default:
    {
        LOG_ERR("Invalid log_level used in output_sensor_data");
    }
    }
}

/******************************************************************************
 * @brief  Checks all ultrasounds to find closest distance to object
 *
 * @param  data - pointer to the sensor data
 *
 * @return Distance to object in meters
 ******************************************************************************/
float closest_distance_to_obstacle(struct Sensor_data* data)
{
    assert(data != NULL);

    float closest_distance = data->forward_ultrasound_left;

    if (data->forward_ultrasound_right < closest_distance)
    {
        closest_distance = data->forward_ultrasound_right;
    }
    else
    {
        // nop
    }

    if (data->left_ultrasound < closest_distance)
    {
        closest_distance = data->left_ultrasound;
    }
    else
    {
        // nop
    }

    if (data->right_ultrasound < closest_distance)
    {
        closest_distance = data->right_ultrasound;
    }
    else
    {
        // nop
    }

    if (data->bottom_ultrasound < closest_distance)
    {
        closest_distance = data->bottom_ultrasound;
    }
    else
    {
        // nop
    }

    if (data->reverse_ultrasound < closest_distance)
    {
        closest_distance = data->reverse_ultrasound;
    }
    else
    {
        // nop
    }

    return closest_distance;
}

/*******************************************************************************
 * @brief Update sensor thread info so that it can be used to calculate
 * priorities
 *
 * @param sensor_thread_info
 * @param data
 *
******************************************************************************/
void update_sensor_thread_info(struct Sensor_thread_info* info,
                               struct Sensor_data*       data)
{
    info->last_run_time = k_uptime_get();
    info->closest_distance = closest_distance_to_obstacle(data);
}
