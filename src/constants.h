// Contains all numerical constants

#pragma once

// System Includes
#include <sys_clock.h>

// Amount of time to sleep for every loop when in dev mode.
static const int DEVELOPMENT_MODE_SLEEP_TIME = 5; // s

// Environment variables
static const float WATER_DENSITY = 998.0; // kg/m^3
static const float GRAVITY       = 9.81; // m/s^2

// AUV parameters
static const float MASS       = 5.0; // kg
static const float LENGTH     = 0.5; // m
static const float WIDTH      = 0.5; // m
static const float HEIGHT     = 0.5; // m
static const float DRAG_COEFF = 0.0; // N
static const float LIFT_COEFF = 0.0; // N
static const float THRUST_MAX = 0.0; // N
static const float THRUST_MIN = 0.0; // N
static const float ADDED_MASS = 0.0; // kg

// Ultrasonic parameters
static const float ULTRASONIC_VARIANCE = 0.0; // m^2
static const float ULTRASONIC_RANGE    = 0.0; // m

// Gyro parameters
static const float GYRO_VARIANCE   = 0.0;// rad^2
static const float LPF_CUTOFF_GYRO = 20; // Hz

// Accelerometer parameters
static const float ACCELEROMETER_VARIANCE = 0.0; // m^2
static const float LPF_CUTOFF_ACCEL       = 20;  // Hz

// Magnetometer parameters
static const float MAGNETOMETER_VARIANCE = 0.0; // m^2

// Pressure parameters
static const float STATIC_PRESSURE_VARIANCE    = 0.0; // m^2
static const float DYNAMIC_PRESSURE_VARIANCE   = 0.0; // m^2
static const float LPF_CUTOFF_STATIC_PRESSURE  = 10;  // Hz
static const float LPF_CUTOFF_DYNAMIC_PRESSURE = 10;  // Hz

// PID parameters
static const float PID_KP = 0.0;
static const float PID_KI = 0.0;
static const float PID_KD = 0.0;

// Kalman filter parameters

// Minimum clearance
static const float MINIMUM_OBSTACLE_CLEARANCE = 0.25; // m

// Distances for thread priority
static const float CLOSE_DISTANCE_THRESHOLD  = 0.5; // m
static const float MEDIUM_DISTANCE_THRESHOLD = 1;   // m
static const float FAR_DISTANCE_THRESHOLD    = 3;   // m

// Maximum time between threads running
static const int MAXIMUM_SENSOR_SLEEP_TIME = 2000; // ms
static const int MAXIMUM_STATE_SLEEP_TIME  = 4000; // ms

// Velocity thresholds for thread priority
static const float NO_VELOCITY_THRESHOLD   = 0.05; // m/s
static const float LOW_VELOCITY_THRESHOLD  = 0.1;  // m/s
static const float HIGH_VELOCITY_THRESHOLD = 1;    // m/s

// Percentages that state can vary. Used for control thread priority.
static const int EMERGENCY_PERCENT_STATE_DIFFERENCE = 20;  // percent
static const int HIGH_PERCENT_STATE_DIFFERENCE      = 15; // percent
static const int MEDIUM_PERCENT_STATE_DIFFERENCE    = 10; // percent
static const int LOW_PERCENT_STATE_DIFFERENCE       = 5; // percent

// Time to wait for mutexes to be unlocked
static const k_timeout_t MAIN_MUTEX_WAIT_TIME       = Z_FOREVER; // ms
static const k_timeout_t SENSOR_MUTEX_WAIT_TIME     = Z_FOREVER; // ms
static const k_timeout_t STATE_MUTEX_WAIT_TIME      = Z_FOREVER; // ms
static const k_timeout_t CONTROL_MUTEX_WAIT_TIME    = Z_FOREVER; // ms
static const k_timeout_t NAVIGATION_MUTEX_WAIT_TIME = Z_FOREVER; // ms

// This means that for every X metres the AUV travels, the calculated percent
// change in the state will be increased by 1. For example, if this value is
// 0.5, then the percent change will be increased by 1 every 0.5 metres.
static const float STATE_METRES_PER_PERCENT_CHANGE = 0.5; // m


// Enumeration Definitions

// Zephyr thread priority is backwards from what we expect. A lower number is
// higher priority.
enum THREAD_PRIORITY
{
    THREAD_PRIORITY_EMERGENCY,
    THREAD_PRIORITY_HIGH,
    THREAD_PRIORITY_MEDIUM,
    THREAD_PRIORITY_LOW,
    THREAD_PRIORITY_OFF
};
