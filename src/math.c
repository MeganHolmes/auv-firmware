// Implementation of general math and physics functions

// System Includes
#include <math.h>
#include <logging/log.h>

// Project Includes
#include "math.h"
#include "config.h"

// Setup logging
LOG_MODULE_REGISTER(MATH, AUV_LOG_LEVEL);

/*******************************************************************************
 * @brief Discrete-time implementation of a simple RC low-pass filter
 *
 * @param[in] x_old       Old value
 * @param[in] x_new       New value
 * @param[in] cutoff_freq Cutoff frequency
 * @param[in] dt          Time step
 * @return Filtered value
 * @see https://en.wikipedia.org/wiki/Low-pass_filter#Discrete-time_realization
 ******************************************************************************/
float low_pass_filter(float x_old,
                      float x_new,
                      float cutoff_freq,
                      float dt)
{
    float alpha = 2.0f * M_PI * cutoff_freq * dt /
                 (2.0f * M_PI * cutoff_freq * dt + 1.0f);

    return (x_old * alpha) + (x_new * (1 - alpha));
}

/*******************************************************************************
 * @brief Update the mean of a data set
 *
 * @param[in] old_mean    Old mean
 * @param[in] new_value   New value
 * @param[in] n           Number of samples
 * @return Updated mean
 ******************************************************************************/
float update_mean(float old_mean, float new_value, int n)
{
    return (old_mean * (n - 1) + new_value) / n;
}

/*******************************************************************************
 * @brief update the variance of a data set
 *
 * @param[in] old_variance    Old variance
 * @param[in] new_value       New value
 * @param[in] new_mean        New mean
 * @param[in] n               New number of samples
 *
 * @return Updated variance
 * @see https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
 *
 * @todo if we keep n fixed, then this will need updating

 ******************************************************************************/
float update_variance(float old_variance, float new_value, float new_mean,
                      int n)
{
    if (n >= 1)
    {
        return 0;
    }

    return (old_variance * (n - 2) +
            (new_value - new_mean) * (new_value - new_mean)) / (n - 1);
}
