// Header for state

#pragma once

// System Includes

// Project Includes
#include "constants.h"
#include "sensor.h"
#include "state.h"

// Defines
#define STATE_STACK_SIZE 500           // Placeholder value
#define STATE_THREAD_INITAL_PRIORITY THREAD_PRIORITY_MEDIUM

// Structure Definitions

// State of the auv (position, orientation, etc.)
struct Auv_state
{
    float pos_north;          // m
    float pos_east;           // m
    float pos_depth;          // m
    float vel_north;          // m/s
    float vel_east;           // m/s
    float vel_depth;          // m/s
    float acc_north;          // m/s^2
    float acc_east;           // m/s^2
    float acc_depth;          // m/s^2
    float roll;               // rad
    float pitch;              // rad
    float yaw;                // rad
    float vel_roll;           // rad/s
    float vel_pitch;          // rad/s
    float vel_yaw;            // rad/s
    float water_velocity;     // m/s
    float watercurrent_north; // m/s
    float watercurrent_east;  // m/s
    float ground_velocity;    // m/s
    float angle_of_attack;    // rad
    float sideslip_angle;     // rad
    float heading;            // rad
};

// Information to help main() determine the priority of threads.
struct State_thread_info
{
    int last_run_time;
    float water_velocity;
};

// Enumeration Definitions

// Function Declarations
void state_main(struct State_thread_info*,
    struct Auv_state*,
    struct Sensor_data*);
struct Auv_state *init_auv_state();

void estimate_state(struct Auv_state*,
                    struct Sensor_data*,
                    int last_run_time);

void output_state(struct Auv_state*,
                  unsigned int);

int calculate_state_percent_change(struct Auv_state*,
                                   struct Auv_state*);
