// Responsible for state estimation for auv.

// System includes
#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <zephyr.h>
#include <logging/log.h>

// Project Includes
#include "config.h"
#include "auv-math.h"
#include "kalman.h"
#include "state.h"

// Functions constrained to this .c file

// setup logging
LOG_MODULE_REGISTER(STATE, AUV_LOG_LEVEL);

// Mutexts
extern struct k_mutex state_mutex;
extern struct k_mutex sensor_data_mutex;

/*******************************************************************************
 * @brief Entry point for state thread
 *
 * @param thread_info pointer to state thread info memory slab
 * @param state       pointer to state memory slab
 * @param sensor_data pointer to sensor data memory slab
 ******************************************************************************/
void state_main(struct State_thread_info* thread_info,
    struct Auv_state* state,
    struct Sensor_data* sensor_data)
{
    LOG_INF("State thread started");

    while (true)
    {
        estimate_state(state, sensor_data, thread_info->last_run_time);
        output_state(state, LOG_LEVEL_DBG);

        thread_info->last_run_time = k_uptime_get();
        thread_info->water_velocity = state->water_velocity;


        LOG_WRN("State Estimation Thread not implemented yet!");
        #ifdef DEVELOPMENT_MODE
        k_sleep(K_SECONDS(DEVELOPMENT_MODE_SLEEP_TIME));
        #endif
        k_yield();
    }

    LOG_ERR("State thread exiting!");
}

/*******************************************************************************
 * @brief Estimates the next state of the AUV based on the current state and
 * sensor data
 *
 * @param state The current state of the AUV
 * @param data          Sensor data to use to estimate the next state
 * @param last_run_time The last time state estimation ran
 ******************************************************************************/
void estimate_state(struct Auv_state*   state_ptr,
                    struct Sensor_data* data_ptr,
                    int                 last_run_time)
{
    assert(state_ptr != NULL);
    assert(data_ptr != NULL);

    LOG_WRN("Method estimate_state not fully implemented!");

    // Copy the data into a local variable to minimize mutex time

    struct Auv_state* state = malloc(sizeof(struct Auv_state));
    struct Sensor_data* data = malloc(sizeof(struct Sensor_data));

    k_mutex_lock(&state_mutex, STATE_MUTEX_WAIT_TIME);
    memcpy(state, state_ptr, sizeof(struct Auv_state));
    k_mutex_unlock(&state_mutex);
    k_mutex_lock(&sensor_data_mutex, STATE_MUTEX_WAIT_TIME);
    memcpy(data, data_ptr, sizeof(struct Sensor_data));
    k_mutex_unlock(&sensor_data_mutex);

    float time_since_last_update = (float)(k_uptime_get() - last_run_time) /
                                   1000.0f;
    // TODO Might need to do something here for the first iteration.

    // --- Angular velocities -------------------------------------------------
    state->vel_roll = low_pass_filter(state->vel_roll,
                                                data->gyro_roll,
                                                LPF_CUTOFF_GYRO,
                                                time_since_last_update);
    state->vel_pitch = low_pass_filter(state->vel_pitch,
                                                data->gyro_pitch,
                                                LPF_CUTOFF_GYRO,
                                                time_since_last_update);
    state->vel_yaw = low_pass_filter(state->vel_yaw,
                                                data->gyro_yaw,
                                                LPF_CUTOFF_GYRO,
                                                time_since_last_update);

    // --- Orientation --------------------------------------------------------

    state->yaw = atan2f(data->mag_x, data->mag_y);

    // NOTE: This probably gives poor results for roll and pitch.
    // Consider switching to a Kalman filter.
    state->roll = atan2f(data->accel_y, data->accel_z);
    state->pitch = asinf(data->accel_x / GRAVITY);

    // Assume sideslip angle = 0 and angle of attack = pitch
    state->sideslip_angle = 0;
    state->heading = state->yaw;
    state->angle_of_attack = state->pitch;

    // --- Acceleration -------------------------------------------------------

    // First, convert accelerometer data to intertial frame.
    float accel_north = (cosf(state->yaw)*data->accel_x +
                        sinf(state->yaw)*data->accel_y) *
                        cosf(state->pitch);
    float accel_east = (sinf(state->yaw)*data->accel_x +
                        cosf(state->yaw)*data->accel_y) *
                        cosf(state->pitch);
    float accel_depth = data->accel_z * sinf(state->pitch);

    // Now low pass filter the acceleration data
    state->acc_north = low_pass_filter(state->acc_north,
                                                accel_north,
                                                LPF_CUTOFF_ACCEL,
                                                time_since_last_update);
    state->acc_east = low_pass_filter(state->acc_east,
                                                accel_east,
                                                LPF_CUTOFF_ACCEL,
                                                time_since_last_update);
    state->acc_depth = low_pass_filter(state->acc_depth,
                                                accel_depth,
                                                LPF_CUTOFF_ACCEL,
                                                time_since_last_update);

    // --- Velocities --------------------------------------------------------

    float dynamic_pressure = sqrtf(2 / (WATER_DENSITY* data->dynamic_pressure));

    state->water_velocity = low_pass_filter(state->water_velocity,
                                            dynamic_pressure,
                                            LPF_CUTOFF_DYNAMIC_PRESSURE,
                                            time_since_last_update);

    state->watercurrent_north = (state->ground_velocity
                                        * cosf(state->heading))
                                        - (state->water_velocity
                                        * cosf(state->yaw));

    state->watercurrent_east  = (state->ground_velocity
                                        * sinf(state->heading))
                                        - (state->water_velocity
                                        * sinf(state->yaw));

    state->vel_north = state->ground_velocity * cosf(state->heading);
    state->vel_east  = state->ground_velocity * sinf(state->heading);
    state->vel_depth = state->ground_velocity * sinf(state->angle_of_attack);

    // Missing ground velocity

    // --- Positions ---------------------------------------------------------
    float depth = data->static_pressure / (WATER_DENSITY * GRAVITY);

    state->pos_depth = low_pass_filter(state->pos_depth,
                                                depth,
                                                LPF_CUTOFF_STATIC_PRESSURE,
                                                time_since_last_update);

    // Missing pos north and pos east but might be able to do it with SLAM


    k_mutex_lock(&state_mutex, STATE_MUTEX_WAIT_TIME);
    memcpy(state_ptr, state, sizeof(struct Auv_state));
    k_mutex_unlock(&state_mutex);
    k_mutex_lock(&sensor_data_mutex, STATE_MUTEX_WAIT_TIME);
    memcpy(data_ptr, data, sizeof(struct Sensor_data));
    k_mutex_unlock(&sensor_data_mutex);

    free(state);
    free(data);
}

/*******************************************************************************
 * @brief Outputs the state to the log
 *
 * @param[in] state     The state to output
 * @param[in] log_level The log level to output to
 ******************************************************************************/
void output_state(struct Auv_state* state,
                  unsigned int      log_level)
{
    assert(state != NULL);

    switch (log_level)
    {
    case LOG_LEVEL_ERR:
    {
        LOG_ERR("--- State ---");
        LOG_ERR("Position X:         %f m",     state->pos_north);
        LOG_ERR("Position Y:         %f m",     state->pos_east);
        LOG_ERR("Position Z:         %f m",     state->pos_depth);
        LOG_ERR("Velocity X:         %f m/s",   state->vel_north);
        LOG_ERR("Velocity Y:         %f m/s",   state->vel_east);
        LOG_ERR("Velocity Z:         %f m/s",   state->vel_depth);
        LOG_ERR("Acceleration X:     %f m/s^2", state->acc_north);
        LOG_ERR("Acceleration Y:     %f m/s^2", state->acc_east);
        LOG_ERR("Acceleration Z:     %f m/s^2", state->acc_depth);
        LOG_ERR("Roll:               %f rad",   state->roll);
        LOG_ERR("Pitch:              %f rad",   state->pitch);
        LOG_ERR("Yaw:                %f rad",   state->yaw);
        LOG_ERR("Roll Velocity:      %f rad/s", state->vel_roll);
        LOG_ERR("Pitch Velocity:     %f rad/s", state->vel_pitch);
        LOG_ERR("Yaw Velocity:       %f rad/s", state->vel_yaw);
        LOG_ERR("Water Velocity:     %f m/s",   state->water_velocity);
        LOG_ERR("Watercurrent X:     %f m/s",   state->watercurrent_north);
        LOG_ERR("Watercurrent Y:     %f m/s",   state->watercurrent_east);
        LOG_ERR("Ground Velocity:    %f m/s",   state->ground_velocity);
        LOG_ERR("Angle of Attack:    %f rad",   state->angle_of_attack);
        LOG_ERR("Sideslip Angle:     %f rad",   state->sideslip_angle);
        LOG_ERR("Heading:            %f rad",   state->heading);

        break;
    }
    case LOG_LEVEL_WRN:
    {
        LOG_WRN("--- State ---");
        LOG_WRN("Position X:         %f m",     state->pos_north);
        LOG_WRN("Position Y:         %f m",     state->pos_east);
        LOG_WRN("Position Z:         %f m",     state->pos_depth);
        LOG_WRN("Velocity X:         %f m/s",   state->vel_north);
        LOG_WRN("Velocity Y:         %f m/s",   state->vel_east);
        LOG_WRN("Velocity Z:         %f m/s",   state->vel_depth);
        LOG_WRN("Acceleration X:     %f m/s^2", state->acc_north);
        LOG_WRN("Acceleration Y:     %f m/s^2", state->acc_east);
        LOG_WRN("Acceleration Z:     %f m/s^2", state->acc_depth);
        LOG_WRN("Roll:               %f rad",   state->roll);
        LOG_WRN("Pitch:              %f rad",   state->pitch);
        LOG_WRN("Yaw:                %f rad",   state->yaw);
        LOG_WRN("Roll Velocity:      %f rad/s", state->vel_roll);
        LOG_WRN("Pitch Velocity:     %f rad/s", state->vel_pitch);
        LOG_WRN("Yaw Velocity:       %f rad/s", state->vel_yaw);
        LOG_WRN("Water Velocity:     %f m/s",   state->water_velocity);
        LOG_WRN("Watercurrent X:     %f m/s",   state->watercurrent_north);
        LOG_WRN("Watercurrent Y:     %f m/s",   state->watercurrent_east);
        LOG_WRN("Ground Velocity:    %f m/s",   state->ground_velocity);
        LOG_WRN("Angle of Attack:    %f rad",   state->angle_of_attack);
        LOG_WRN("Sideslip Angle:     %f rad",   state->sideslip_angle);
        LOG_WRN("Heading:            %f rad",   state->heading);

        break;
    }
    case LOG_LEVEL_INF:
    {
        LOG_INF("--- State ---");
        LOG_INF("Position X:         %f m",     state->pos_north);
        LOG_INF("Position Y:         %f m",     state->pos_east);
        LOG_INF("Position Z:         %f m",     state->pos_depth);
        LOG_INF("Velocity X:         %f m/s",   state->vel_north);
        LOG_INF("Velocity Y:         %f m/s",   state->vel_east);
        LOG_INF("Velocity Z:         %f m/s",   state->vel_depth);
        LOG_INF("Acceleration X:     %f m/s^2", state->acc_north);
        LOG_INF("Acceleration Y:     %f m/s^2", state->acc_east);
        LOG_INF("Acceleration Z:     %f m/s^2", state->acc_depth);
        LOG_INF("Roll:               %f rad",   state->roll);
        LOG_INF("Pitch:              %f rad",   state->pitch);
        LOG_INF("Yaw:                %f rad",   state->yaw);
        LOG_INF("Roll Velocity:      %f rad/s", state->vel_roll);
        LOG_INF("Pitch Velocity:     %f rad/s", state->vel_pitch);
        LOG_INF("Yaw Velocity:       %f rad/s", state->vel_yaw);
        LOG_INF("Water Velocity:     %f m/s",   state->water_velocity);
        LOG_INF("Watercurrent X:     %f m/s",   state->watercurrent_north);
        LOG_INF("Watercurrent Y:     %f m/s",   state->watercurrent_east);
        LOG_INF("Ground Velocity:    %f m/s",   state->ground_velocity);
        LOG_INF("Angle of Attack:    %f rad",   state->angle_of_attack);
        LOG_INF("Sideslip Angle:     %f rad",   state->sideslip_angle);
        LOG_INF("Heading:            %f rad",   state->heading);

        break;
    }
    case LOG_LEVEL_DBG:
    {
        LOG_DBG("--- State ---");
        LOG_DBG("Position X:         %f m",     state->pos_north);
        LOG_DBG("Position Y:         %f m",     state->pos_east);
        LOG_DBG("Position Z:         %f m",     state->pos_depth);
        LOG_DBG("Velocity X:         %f m/s",   state->vel_north);
        LOG_DBG("Velocity Y:         %f m/s",   state->vel_east);
        LOG_DBG("Velocity Z:         %f m/s",   state->vel_depth);
        LOG_DBG("Acceleration X:     %f m/s^2", state->acc_north);
        LOG_DBG("Acceleration Y:     %f m/s^2", state->acc_east);
        LOG_DBG("Acceleration Z:     %f m/s^2", state->acc_depth);
        LOG_DBG("Roll:               %f rad",   state->roll);
        LOG_DBG("Pitch:              %f rad",   state->pitch);
        LOG_DBG("Yaw:                %f rad",   state->yaw);
        LOG_DBG("Roll Velocity:      %f rad/s", state->vel_roll);
        LOG_DBG("Pitch Velocity:     %f rad/s", state->vel_pitch);
        LOG_DBG("Yaw Velocity:       %f rad/s", state->vel_yaw);
        LOG_DBG("Water Velocity:     %f m/s",   state->water_velocity);
        LOG_DBG("Watercurrent X:     %f m/s",   state->watercurrent_north);
        LOG_DBG("Watercurrent Y:     %f m/s",   state->watercurrent_east);
        LOG_DBG("Ground Velocity:    %f m/s",   state->ground_velocity);
        LOG_DBG("Angle of Attack:    %f rad",   state->angle_of_attack);
        LOG_DBG("Sideslip Angle:     %f rad",   state->sideslip_angle);
        LOG_DBG("Heading:            %f rad",   state->heading);

        break;
    }
    default:
    {
        LOG_ERR("Invalid log_level used in output_state!");
    }
    }
}

/*******************************************************************************
 * @brief Calculate the percent change from two inputs states.
 *
 * @param current_state  New state to compare
 * @param previous_state Previous tate to compare against.
 * @return percent change of new state from previous state
 ******************************************************************************/
int calculate_state_percent_change(struct Auv_state *current_state,
                                   struct Auv_state *previous_state)
{
    assert(current_state != NULL);
    assert(previous_state != NULL);

    int percent_change = 0;

    // Position doesn't make a ton of sense for percent change but it should
    // still be a factor. For every X meter of change, we add 1% to the
    // percent change. X = STATE_METRES_PER_PERCENT_CHANGE constant
    float distance_in_3d = calculate_3d_distance(current_state->pos_north,
                                                 current_state->pos_east,
                                                 current_state->pos_depth,
                                                 previous_state->pos_north,
                                                 previous_state->pos_east,
                                                 previous_state->pos_depth);


    percent_change = roundf(distance_in_3d / STATE_METRES_PER_PERCENT_CHANGE);

    // Note: For velocity and acceleration I was going to calculate the 3D
    // vectors and take the differece between them. However, this is not
    // a good idea because if the total velocity stayed the same, but the
    // direction changed, the percent change would be 0 but this would be
    // a significant change that would warrant thread switching.

    // Velocity
    percent_change += calculate_percent_change(current_state->vel_north,
                                               previous_state->vel_north);
    percent_change += calculate_percent_change(current_state->vel_east,
                                                  previous_state->vel_east);
    percent_change += calculate_percent_change(current_state->vel_depth,
                                                    previous_state->vel_depth);

    // Acceleration
    percent_change += calculate_percent_change(current_state->acc_north,
                                               previous_state->acc_north);
    percent_change += calculate_percent_change(current_state->acc_east,
                                                  previous_state->acc_east);
    percent_change += calculate_percent_change(current_state->acc_depth,
                                                    previous_state->acc_depth);


    // Roll, Pitch, Yaw
    percent_change += calculate_percent_change(current_state->roll,
                                               previous_state->roll);
    percent_change += calculate_percent_change(current_state->pitch,
                                               previous_state->pitch);
    percent_change += calculate_percent_change(current_state->yaw,
                                               previous_state->yaw);

    // Roll Velocity, Pitch Velocity, Yaw Velocity
    percent_change += calculate_percent_change(current_state->vel_roll,
                                               previous_state->vel_roll);
    percent_change += calculate_percent_change(current_state->vel_pitch,
                                               previous_state->vel_pitch);
    percent_change += calculate_percent_change(current_state->vel_yaw,
                                               previous_state->vel_yaw);

    // After this point the following calculations are derivied from others and
    // could inflat the percent change unneccesarily.

    // Water Velocity
    percent_change += calculate_percent_change(current_state->water_velocity,
                                               previous_state->water_velocity);

    // Watercurrent
    percent_change += calculate_percent_change(
        current_state->watercurrent_north,
        previous_state->watercurrent_north);
    percent_change += calculate_percent_change(
        current_state->watercurrent_east,
        previous_state->watercurrent_east);

    // Ground Velocity
    percent_change += calculate_percent_change(current_state->ground_velocity,
                                               previous_state->ground_velocity);

    // Angle of Attack
    percent_change += calculate_percent_change(current_state->angle_of_attack,
                                               previous_state->angle_of_attack);

    // Sideslip angle
    percent_change += calculate_percent_change(current_state->sideslip_angle,
                                               previous_state->sideslip_angle);

    // Heading
    percent_change += calculate_percent_change(current_state->heading,
                                             previous_state->heading);

    LOG_DBG("State percent change is %d", percent_change);
    return percent_change;
}
