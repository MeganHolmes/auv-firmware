// Implementation of general math and physics functions

// System Includes
#include <assert.h>
#include <logging/log.h>
#include <zephyr.h>

// Project Includes
#include "config.h"
#include "thread.h"


// Setup logging
LOG_MODULE_REGISTER(THREAD, AUV_LOG_LEVEL);

/*******************************************************************************
 * @brief Calculates the priority of the sensor thread.
 *
 * @param time     The time in milliseconds since the last sensor update.
 * @param distance The distance to the nearest obstacle.
 * @param velocity The current velocity of the AUV.
 *
 * @return The new priority of the sensor thread.
 ******************************************************************************/
enum THREAD_PRIORITY calculate_sensor_priority(
    int   time,
    float distance,
    float velocity)
{
    int delta_t_sensor = k_uptime_get() - time;

    // Sensor thread will update its own priority if it detects a potential
    // collision but this will keep it at 0 while updating other threads.
    if (check_for_thread_emergency(
        distance,
        MINIMUM_OBSTACLE_CLEARANCE,
        delta_t_sensor,
        MAXIMUM_SENSOR_SLEEP_TIME))
    {
        LOG_WRN("Sensor thread emergency condition detected.");
        return THREAD_PRIORITY_EMERGENCY;
    }
    else
    {
        LOG_DBG("Sensor thread not in emergency condition.");
    }


    enum THREAD_PRIORITY highest_priority = THREAD_PRIORITY_OFF;

    calculate_time_based_priority(
        delta_t_sensor, MAXIMUM_SENSOR_SLEEP_TIME, &highest_priority);

    if (highest_priority == THREAD_PRIORITY_HIGH)
    {
        return highest_priority;
        LOG_DBG("Sensor thread priority set to %d", highest_priority);
    }
    else
    {
        //nop
    }


    calculate_distance_based_priority(distance, &highest_priority);

    if (highest_priority == THREAD_PRIORITY_HIGH)
    {
        return highest_priority;
        LOG_DBG("Sensor thread priority set to %d", highest_priority);
    }
    else
    {
        // nop
    }

    calculate_velocity_based_priority(velocity, &highest_priority);


    LOG_DBG("Sensor thread priority set to %d", highest_priority);
    return highest_priority;
}

/*******************************************************************************
 * @brief Calculates the priority of the state thread.
 *
 * @return The new priority of the state thread.
 ******************************************************************************/
enum THREAD_PRIORITY calculate_state_priority(
    int                  time,
    float                distance,
    enum THREAD_PRIORITY control_priority)
{

    int delta_t_state = k_uptime_get() - time;

    // Sensor thread will update its this priority if it detects a potential
    // collision but this will keep it at 0 while updating other threads.
    if (check_for_thread_emergency(
        distance,
        MINIMUM_OBSTACLE_CLEARANCE,
        delta_t_state,
        MAXIMUM_SENSOR_SLEEP_TIME))
    {
        LOG_WRN("State thread emergency condition detected.");
        return THREAD_PRIORITY_EMERGENCY;
    }
    else
    {
        LOG_DBG("State thread not in emergency condition.");
    }


    enum THREAD_PRIORITY highest_priority = THREAD_PRIORITY_LOW;

    calculate_time_based_priority(
        delta_t_state, MAXIMUM_STATE_SLEEP_TIME, &highest_priority);

    if (highest_priority == THREAD_PRIORITY_HIGH)
    {
        return highest_priority;
        LOG_DBG("State thread priority set to %d", highest_priority);
    }
    else
    {
        // nop
    }

    calculate_distance_based_priority(distance, &highest_priority);

    if (highest_priority == THREAD_PRIORITY_HIGH)
    {
        return highest_priority;
        LOG_DBG("State thread priority set to %d", highest_priority);
    }
    else
    {
        // nop
    }

    calculate_control_based_state_priority(control_priority, &highest_priority);

    LOG_DBG("State thread priority set to %d", highest_priority);
    return highest_priority;
}

/*******************************************************************************
 * @brief Calculates the priority of the control thread.
 *
 * @param time                         The time in milliseconds since the last
 *                                     control update.
 * @param distance                     The distance to the nearest obstacle.
 * @param percent_state_change         The percent change in state since
 *                                     control last ran.
 * @param percent_desired_state_change The percent change in desired
 *                                     state since control last ran.
 *
 * @return The new priority of the control thread.
 ******************************************************************************/
enum THREAD_PRIORITY calculate_control_priority(
    int   time,
    float distance,
    int   percent_state_change,
    int   percent_desired_state_change)
{
    int delta_t_control = k_uptime_get() - time;

    // Sensor thread will update its this priority if it detects a potential
    // collision but this will keep it at 0 while updating other threads.
    if (check_for_thread_emergency(
        distance,
        MINIMUM_OBSTACLE_CLEARANCE,
        delta_t_control,
        MAXIMUM_SENSOR_SLEEP_TIME))
    {
        LOG_WRN("Control thread emergency condition detected.");
        return THREAD_PRIORITY_EMERGENCY;
    }
    else
    {
        LOG_DBG("Control thread not in emergency condition.");
    }

    if (percent_state_change > EMERGENCY_PERCENT_STATE_DIFFERENCE) // > 20% diff
    {
        LOG_WRN("High state difference detected. Control thread set to %s",
            " emergency");
        return THREAD_PRIORITY_EMERGENCY;
    }
    else
    {
        // nop
    }

    enum THREAD_PRIORITY highest_priority = THREAD_PRIORITY_LOW;

    calculate_time_based_priority(
        delta_t_control, MAXIMUM_STATE_SLEEP_TIME, &highest_priority);

    if (highest_priority == THREAD_PRIORITY_HIGH)
    {
        return highest_priority;
        LOG_DBG("Control thread priority set to %d", highest_priority);
    }
    else
    {
        // nop
    }

    calculate_distance_based_priority(distance, &highest_priority);

    if (highest_priority == THREAD_PRIORITY_HIGH)
    {
        return highest_priority;
        LOG_DBG("Control thread priority set to %d", highest_priority);
    }
    else
    {
        // nop
    }

    calculate_percent_based_priority(
        percent_state_change, &highest_priority);

    if (highest_priority == THREAD_PRIORITY_HIGH)
    {
        return highest_priority;
        LOG_DBG("Control thread priority set to %d", highest_priority);
    }
    else
    {
        // nop
    }

    calculate_percent_based_priority(
        percent_desired_state_change, &highest_priority);


    LOG_DBG("Control thread priority set to %d", highest_priority);
    return highest_priority;
}

/*******************************************************************************
 * @brief Checks if distance and time based emergency conditions are met.
 *
 * @param distance     Last distance seen.
 * @param min_distance Minimum distance to consider an obstacle.
 * @param time         Time since last update.
 * @param max_time     Maximum time between updates.
 *
 * @return True if the conditions are met, false otherwise.
 ******************************************************************************/
bool check_for_thread_emergency(
    float distance,
    float min_distance,
    int time,
    int max_time)
{
    if (distance < min_distance)
    {
        LOG_WRN("Potential collision detected");
        return true;
    }
    else
    {
        LOG_DBG("No potential collision detected");
    }

    if (time > max_time)
    {
        LOG_WRN("Thread has not run in the last %d seconds", max_time/1000);
        return true;
    }
    else
    {
        LOG_DBG("Thread has run in the last %d seconds", max_time/1000);
    }

    return false;
}

/*******************************************************************************
 * @brief Determines which priority zone the time since last run falls in.
 *
 * @param time             Time since last run.
 * @param max_time         Maximum time between runs.
 * @param highest_priority The highest priority so far.
*******************************************************************************/
void calculate_time_based_priority(
    int                   time,
    int                   max_time,
    enum THREAD_PRIORITY* highest_priority)
{
    if (time > 3*max_time/4) // > 75% max time
    {
        memset(highest_priority, THREAD_PRIORITY_HIGH,
         sizeof(enum THREAD_PRIORITY));
    }
    else if (time > 2*max_time/4) // > 50% max time
    {
        memset(highest_priority, THREAD_PRIORITY_MEDIUM,
         sizeof(enum THREAD_PRIORITY));
    }
    else if (time > max_time/4 &&
        *highest_priority == THREAD_PRIORITY_OFF) //>25%
    {
        memset(highest_priority, THREAD_PRIORITY_LOW,
         sizeof(enum THREAD_PRIORITY));
    }
    else
    {
        // Else stay at previous priority
    }

}

/*******************************************************************************
 * @brief Determines which priority zone the distance falls in.
 *
 * @param distance         Last distance seen.
 * @param highest_priority The highest priority so far.
 *
 ******************************************************************************/
void calculate_distance_based_priority(
    float distance, enum THREAD_PRIORITY* highest_priority)
{
    if (distance < CLOSE_DISTANCE_THRESHOLD)
    {
        memset(highest_priority, THREAD_PRIORITY_HIGH,
         sizeof(enum THREAD_PRIORITY));
    }
    else if (distance < MEDIUM_DISTANCE_THRESHOLD)
    {
        memset(highest_priority, THREAD_PRIORITY_MEDIUM,
         sizeof(enum THREAD_PRIORITY));
    }
    else if (distance < FAR_DISTANCE_THRESHOLD &&
             *highest_priority == THREAD_PRIORITY_OFF)
    {
        memset(highest_priority, THREAD_PRIORITY_LOW,
         sizeof(enum THREAD_PRIORITY));
    }
    else
    {
        // Else stay at previous priority
    }
}

/*******************************************************************************
 * @brief Determines which priority zone the velocity falls in.
 *
 * @param velocity         Last velocity seen.
 * @param highest_priority The highest priority so far.
 *
 ******************************************************************************/
void calculate_velocity_based_priority(
    float                 velocity,
    enum THREAD_PRIORITY* highest_priority)
{
    if (velocity > HIGH_VELOCITY_THRESHOLD)
    {
        memset(highest_priority, THREAD_PRIORITY_HIGH,
         sizeof(enum THREAD_PRIORITY));
    }
    else if (velocity > LOW_VELOCITY_THRESHOLD)
    {
        memset(highest_priority, THREAD_PRIORITY_MEDIUM,
         sizeof(enum THREAD_PRIORITY));
    }
    else if (velocity > NO_VELOCITY_THRESHOLD &&
             *highest_priority == THREAD_PRIORITY_OFF)
    {
        memset(highest_priority, THREAD_PRIORITY_LOW,
         sizeof(enum THREAD_PRIORITY));
    }
    else
    {
        // Else stay at previous priority
    }
}

/*******************************************************************************
 * @brief Calculates what priority state should be based on control priority
 *
 * @param control_priority The priority of the control thread.
 * @param highest_priority The priority of the state thread.
 *
 ******************************************************************************/
void calculate_control_based_state_priority(
    enum THREAD_PRIORITY  control_priority,
    enum THREAD_PRIORITY* highest_priority)
{
    if (control_priority == THREAD_PRIORITY_EMERGENCY)
    {
        memset(highest_priority, THREAD_PRIORITY_HIGH,
         sizeof(enum THREAD_PRIORITY));
    }
    else if (control_priority == THREAD_PRIORITY_HIGH)
    {
        memset(highest_priority, THREAD_PRIORITY_MEDIUM,
         sizeof(enum THREAD_PRIORITY));
    }
    else if (control_priority == THREAD_PRIORITY_MEDIUM &&
             *highest_priority == THREAD_PRIORITY_OFF)
    {
        memset(highest_priority, THREAD_PRIORITY_LOW,
         sizeof(enum THREAD_PRIORITY));
    }
    else
    {
        // Else stay at previous priority
    }
}

/*******************************************************************************
 * @brief Calculate priority based on percent difference of state
 *
 * @param percent          The percent difference of state.
 * @param highest_priority The priority of the control thread.
 ******************************************************************************/
void calculate_percent_based_priority(
    int                   percent,
    enum THREAD_PRIORITY* highest_priority)
{
    if (percent > HIGH_PERCENT_STATE_DIFFERENCE)
    {
        memset(highest_priority, THREAD_PRIORITY_HIGH,
         sizeof(enum THREAD_PRIORITY));
    }
    else if (percent > MEDIUM_PERCENT_STATE_DIFFERENCE)
    {
        memset(highest_priority, THREAD_PRIORITY_MEDIUM,
         sizeof(enum THREAD_PRIORITY));
    }
    else if (percent > LOW_PERCENT_STATE_DIFFERENCE &&
             *highest_priority == THREAD_PRIORITY_OFF)
    {
        memset(highest_priority, THREAD_PRIORITY_LOW,
         sizeof(enum THREAD_PRIORITY));
    }
    else
    {
        // Else stay at previous priority
    }
}
