// Header for general math and physics functions

#pragma once

// System Includes

// Project Includes

// Structure Definitions

// Enumeration Definitions

// Function Declarations

float low_pass_filter(float x_old,
                      float x_new,
                      float cutoff_freq,
                      float dt);

float calculate_3d_distance(float x1,
                            float y1,
                            float z1,
                            float x2,
                            float y2,
                            float z2);

float calculate_3d_vector_length(float x,
                                 float y,
                                 float z);

int calculate_percent_change(float x_old,
                             float x_new);
