// This file is responsible for the translation of desired states into actions
// and executes those actions.

#pragma once

// System Includes

// Project Includes
#include "constants.h"
#include "state.h"

// Defines
#define CONTROL_STACK_SIZE 500           // Placeholder value
#define CONTROL_THREAD_INITAL_PRIORITY THREAD_PRIORITY_MEDIUM

// Structure Definitions

// Commands. Each float is a command for a single actuator.
struct Commands {
        float left_aileron;  // rad (-pi/2 to pi/2)
        float right_aileron; // rad (-pi/2 to pi/2)
        float elevator;      // rad (-pi/2 to pi/2)
        float rudder;        // rad (-pi/2 to pi/2)
        float throttle;      // percent (0 to 1)
};

struct Control_thread_info {
    int last_run_time;
};

struct Control_state_memory_pointers {
    struct AUV_state* current_state;
    struct AUV_state* desired_state;
    struct AUV_state* previous_state;
};

// Enumeration Definitions

// Function Declarations
void control_main(struct Control_thread_info*,
    struct Control_state_memory_pointers*);

struct Commands* translate_desired_state_to_commands(
    struct Auv_state* current_state,
    struct Auv_state* desired_state);

bool execute_commands(struct Commands* actions);

void output_commands(struct Commands* actions,
                     unsigned int     log_level);
