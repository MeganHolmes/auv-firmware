// Methods to interact with sensors.

#pragma once

// System Includes

// Project Includes
#include "constants.h"

// Defines
#define SENSOR_STACK_SIZE 500           // Placeholder value
#define SENSOR_THREAD_INITAL_PRIORITY THREAD_PRIORITY_HIGH

// Structure Definitions

struct Sensor_data {
        float forward_ultrasound_left;  // m
        float forward_ultrasound_right; // m
        float left_ultrasound;          // m
        float right_ultrasound;         // m
        float bottom_ultrasound;        // m
        float reverse_ultrasound;       // m

        float gyro_roll; // rad/s
        float gyro_pitch; // rad/s
        float gyro_yaw; // rad/s

        float accel_x; // m/s^2
        float accel_y; // m/s^2
        float accel_z; // m/s^2

        float mag_x; // Mx
        float mag_y; // Mx
        float mag_z; // Mx

        float static_pressure;  // Pa
        float dynamic_pressure; // Pa
        float temperature;      // K
};

// Information to help main() determine the priority of the sensor thread.
struct Sensor_thread_info
{
        int last_run_time;
        float closest_distance;
};


// Enumeration Definitions

// Function Declarations
void sensor_main(struct Sensor_thread_info*,
        struct Sensor_data*, void*);
void retrieve_sensor_data(struct Sensor_data* data);
bool check_for_possible_collision(struct Sensor_data* data);

void output_sensor_data(struct Sensor_data* data,
                        unsigned int        log_level);

float closest_distance_to_obstacle(struct Sensor_data* data);

void update_sensor_thread_info(struct Sensor_thread_info* info,
                               struct Sensor_data*       data);
