// Methods to calculate thread priorities

#pragma once

// System Includes

// Project Includes
#include "constants.h"
#include "sensor.h"
#include "control.h"
#include "state.h"
#include "navigate.h"

// Defines

// Structure Definitions

// Enumeration Definitions

// Function Declarations
enum THREAD_PRIORITY calculate_sensor_priority(int, float, float);
enum THREAD_PRIORITY calculate_state_priority(int, float, enum THREAD_PRIORITY);
enum THREAD_PRIORITY calculate_control_priority(int, float, int, int);
bool check_for_thread_emergency(float, float, int, int);
void calculate_time_based_priority(int, int, enum THREAD_PRIORITY*);
void calculate_distance_based_priority(float, enum THREAD_PRIORITY*);
void calculate_velocity_based_priority(float, enum THREAD_PRIORITY*);
void calculate_control_based_state_priority(enum THREAD_PRIORITY,
     enum THREAD_PRIORITY*);
void calculate_percent_based_priority(int, enum THREAD_PRIORITY*);
