// This file is responsible for determining the desired state of the system

#pragma once

// System Includes

// Project Includes
#include "constants.h"
#include "state.h"

// Defines
#define NAVIGATE_STACK_SIZE 500           // Placeholder value
#define NAVIGATE_THREAD_INITAL_PRIORITY THREAD_PRIORITY_LOW

// Structure Definitions

struct Navigate_thread_info {

    int last_run_time;
};

// Enumeration Definitions

void navigate_main(void*, void*, void*);

struct Auv_state* avoid_collision(struct Auv_state*   current_state,
                                  struct Sensor_data* data);
