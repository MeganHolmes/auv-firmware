// Main Loop for auv-firmware.

// System includes
#include <stdlib.h>
#include <logging/log.h>
#include <zephyr.h>

// Project includes
#include "constants.h"
#include "config.h"
#include "state.h"
#include "sensor.h"
#include "control.h"
#include "navigate.h"
#include "math.h"
#include "thread.h"

// Functions constrained to this .c file
bool init_mem_slabs(
    struct Sensor_thread_info*,
    struct State_thread_info*,
    struct Control_thread_info*,
    struct Navigate_thread_info*,
    struct Sensor_data*,
    struct Auv_state*,
    struct Auv_state*,
    struct Auv_state*,
    struct Auv_state*);


// Setup logging
LOG_MODULE_REGISTER(MAIN, AUV_LOG_LEVEL);

// Setup threads
K_THREAD_STACK_DEFINE(sensor_stack_area, SENSOR_STACK_SIZE);
K_THREAD_STACK_DEFINE(state_stack_area, STATE_STACK_SIZE);
K_THREAD_STACK_DEFINE(control_stack_area, CONTROL_STACK_SIZE);
K_THREAD_STACK_DEFINE(navigate_stack_area, NAVIGATE_STACK_SIZE);

// Setup memory slabs

// Slabs containing basic information used for priority calculation
K_MEM_SLAB_DEFINE(sensor_thread_mem, sizeof(struct Sensor_thread_info), 1, 4);
K_MEM_SLAB_DEFINE(state_thread_mem, sizeof(struct State_thread_info), 1, 4);
K_MEM_SLAB_DEFINE(nav_thread_mem, sizeof(struct Navigate_thread_info), 1, 4);
K_MEM_SLAB_DEFINE(control_thread_mem, sizeof(struct Control_thread_info), 1, 4);

// Slabs containing larger amounts of data
K_MEM_SLAB_DEFINE(sensor_data_mem, sizeof(struct Sensor_data), 1, 4);
K_MEM_SLAB_DEFINE(state_mem, sizeof(struct Auv_state), 1, 4);
K_MEM_SLAB_DEFINE(desired_state_mem, sizeof(struct Auv_state), 1, 4);
K_MEM_SLAB_DEFINE(previous_state_mem, sizeof(struct Auv_state), 1, 4);
K_MEM_SLAB_DEFINE(previous_desired_state_mem, sizeof(struct Auv_state), 1, 4);

// Setup Mutexs
K_MUTEX_DEFINE(sensor_data_mutex);
K_MUTEX_DEFINE(state_mutex);
K_MUTEX_DEFINE(desired_state_mutex);
K_MUTEX_DEFINE(previous_state_mutex);
K_MUTEX_DEFINE(previous_desired_state_mutex);

void main(void)
{
    LOG_INF("Initializing memory slabs");

    struct Sensor_thread_info* sensor_thread_info = malloc(
        sizeof(struct Sensor_thread_info));
    struct State_thread_info* state_thread_info = malloc(
        sizeof(struct State_thread_info));
    struct Control_thread_info* control_thread_info = malloc(
        sizeof(struct Control_thread_info));
    struct Navigate_thread_info* navigate_thread_info = malloc(
        sizeof(struct Navigate_thread_info));

    struct Sensor_data* sensor_data = malloc(sizeof(struct Sensor_data));
    struct Auv_state* state = malloc(sizeof(struct Auv_state));
    struct Auv_state* desired_state = malloc(sizeof(struct Auv_state));
    struct Auv_state* previous_state = malloc(sizeof(struct Auv_state));
    struct Auv_state* previous_desired_state = malloc(sizeof(struct Auv_state));

    if (!init_mem_slabs(sensor_thread_info,
        state_thread_info,
        control_thread_info,
        navigate_thread_info,
        sensor_data,
        state,
        desired_state,
        previous_state,
        previous_desired_state))
    {
        // If memory allocation fails, we will end up crashing so anyway so just
        // terminate now.
        LOG_ERR("Failed to initialize memory slabs. Exiting.");
        return;
    }
    else
    {
        LOG_INF("Memory slabs initialized");
    }

    struct Control_state_memory_pointers control_state_mem_ptrs;
    control_state_mem_ptrs.current_state  = state;
    control_state_mem_ptrs.previous_state = previous_state;
    control_state_mem_ptrs.desired_state  = desired_state;

    LOG_INF("Spawning threads");

    struct k_thread sensor_thread;
    struct k_thread state_thread;
    struct k_thread control_thread;
    struct k_thread navigate_thread;

    // Everytime I tried wrapping these into a function i would get seg faults

    k_tid_t sensor_thread_id = k_thread_create(
        &(sensor_thread),                  // Give uninitialized k_thread struct
        sensor_stack_area,                 // Give stack area
        K_THREAD_STACK_SIZEOF(sensor_stack_area), // Don't change
        sensor_main,                       // Main function for new thread
        sensor_thread_info,                // Arguments for main function
        sensor_data,                       // Arguments for main function
        NULL,                              // Arguments for main function
        SENSOR_THREAD_INITAL_PRIORITY,     // Starting priority
        K_ESSENTIAL,             // If the thread aborts, mark as critical error
        K_NO_WAIT);                        // delay until thread is started

    k_tid_t state_thread_id = k_thread_create(
        &(state_thread),                   // Give uninitialized k_thread struct
        state_stack_area,                  // Give stack area
        K_THREAD_STACK_SIZEOF(state_stack_area), // Don't change
        state_main,                        // Main function for new thread
        state_thread_info,                 // Arguments for main function
        state,                             // Arguments for main function
        sensor_data,                       // Arguments for main function
        STATE_THREAD_INITAL_PRIORITY,      // Starting priority
        K_ESSENTIAL,             // If the thread aborts, mark as critical error
        K_NO_WAIT);                        // delay until thread is started

    k_tid_t control_thread_id = k_thread_create(
        &(control_thread),                 // Give uninitialized k_thread struct
        control_stack_area,                // Give stack area
        K_THREAD_STACK_SIZEOF(control_stack_area), // Don't change
        control_main,                      // Main function for new thread
        control_thread_info,               // Arguments for main function
        &control_state_mem_ptrs,            // Arguments for main function
        NULL,
        CONTROL_THREAD_INITAL_PRIORITY,    // Starting priority
        K_ESSENTIAL,             // If the thread aborts, mark as critical error
        K_NO_WAIT);                        // delay until thread is started

#if false
    k_tid_t navigate_thread_id = k_thread_create(
        &(navigate_thread),                // Give uninitialized k_thread struct
        navigate_stack_area,               // Give stack area
        K_THREAD_STACK_SIZEOF(navigate_stack_area), // Don't change
        navigate_main,                     // Main function for new thread
        NULL,                              // Arguments for main function
        NULL,                              // Arguments for main function
        NULL,                              // Arguments for main function
        NAVIGATE_THREAD_INITAL_PRIORITY,   // Starting priority
        K_ESSENTIAL,             // If the thread aborts, mark as critical error
        K_NO_WAIT);                        // delay until thread is started
#endif

    LOG_INF("Threads spawned");

    LOG_INF("Main initialization complete");

    k_yield(); // TODO: replace with sleep or something

    struct Auv_state* cached_state = malloc(
        sizeof(struct Auv_state));
    struct Auv_state* cached_previous_state = malloc(
        sizeof(struct Auv_state));
    struct Auv_state* cached_desired_state = malloc(
        sizeof(struct Auv_state));
    struct Auv_state* cached_previous_desired_state = malloc(
        sizeof(struct Auv_state));

    // The rest of this thread only updates the priority of the other
    // threads
    while (true)
    {

        // --- Update Sensor Thread Priority -----------------------------------
        k_thread_priority_set(sensor_thread_id,
            (int)calculate_sensor_priority(
                sensor_thread_info->last_run_time,
                sensor_thread_info->closest_distance,
                state_thread_info->water_velocity));

        // --- Update Control Thread Priority ----------------------------------

        k_mutex_lock(&state_mutex, MAIN_MUTEX_WAIT_TIME);
        memcpy(cached_state, state,
            sizeof(struct Auv_state));
        k_mutex_unlock(&state_mutex);

        k_mutex_lock(&previous_state_mutex, MAIN_MUTEX_WAIT_TIME);
        memcpy(cached_previous_state, previous_state,
            sizeof(struct Auv_state));
        k_mutex_unlock(&previous_state_mutex);

        k_mutex_lock(&desired_state_mutex, MAIN_MUTEX_WAIT_TIME);
        memcpy(cached_desired_state, desired_state,
            sizeof(struct Auv_state));
        k_mutex_unlock(&desired_state_mutex);

        k_mutex_lock(&previous_desired_state_mutex, MAIN_MUTEX_WAIT_TIME);
        memcpy(cached_previous_desired_state, previous_desired_state,
            sizeof(struct Auv_state));
        k_mutex_unlock(&previous_desired_state_mutex);


        int percent_state_change = calculate_state_percent_change(
            cached_state, cached_previous_state);
        int percent_desired_state_change = calculate_state_percent_change(
            cached_desired_state, cached_previous_desired_state);

        enum THREAD_PRIORITY control_priority = calculate_control_priority(
            control_thread_info->last_run_time,
            sensor_thread_info->closest_distance,
            percent_state_change,
            percent_desired_state_change);

        k_thread_priority_set(control_thread_id, (int)control_priority);

        // --- Update State Thread Priority ------------------------------------

        k_thread_priority_set(state_thread_id, (int)calculate_state_priority(
            state_thread_info->last_run_time,
            sensor_thread_info->closest_distance,
            control_priority));


        LOG_WRN("Priority update not implemented yet!");

        #ifdef DEVELOPMENT_MODE
        k_sleep(K_SECONDS(DEVELOPMENT_MODE_SLEEP_TIME));
        #endif
        k_yield();
    }

    LOG_ERR("Exited main thread, program terminated");
    exit(0);
}

/*******************************************************************************
 * @brief Initialize memory slabs
 *
 * @param sensor_thread_info     Pointer to the sensor thread info struct
 * @param state_thread_info      Pointer to the state thread info struct
 * @param control_thread_info    Pointer to the control thread info struct
 * @param navigate_thread_info   Pointer to the navigate thread info struct
 * @param sensor_data            Pointer to the sensor data struct
 * @param state                  Pointer to the state struct
 * @param desired_state          Pointer to the desired state struct
 * @param previous_state         Pointer to the previous state struct
 * @param previous_desired_state Pointer to the previous desired state struct
 *
 * @return true on success, false on failure
 ******************************************************************************/
bool init_mem_slabs(
    struct Sensor_thread_info*   sensor_thread_info,
    struct State_thread_info*    state_thread_info,
    struct Control_thread_info*  control_thread_info,
    struct Navigate_thread_info* navigate_thread_info,
    struct Sensor_data*          sensor_data,
    struct Auv_state*            state,
    struct Auv_state*            desired_state,
    struct Auv_state*            previous_state,
    struct Auv_state*            previous_desired_state)
{
    if (k_mem_slab_alloc(&sensor_thread_mem, &sensor_thread_info, K_NO_WAIT)
         != 0)
    {
        LOG_ERR("Sensor thread memory slab failed to allocate!");
        return false;
    }
    else
    {
        LOG_DBG("Sensor thread memory slab allocated!");
    }

    if (k_mem_slab_alloc(&state_thread_mem, &state_thread_info, K_NO_WAIT)
     != 0)
    {
        LOG_ERR("State thread memory slab failed to allocate!");
        return false;
    }
    else
    {
        LOG_DBG("State thread memory slab allocated!");
    }

    if (k_mem_slab_alloc(&nav_thread_mem, &navigate_thread_info, K_NO_WAIT)
     != 0)
    {
        LOG_ERR("Navigation thread memory slab failed to allocate!");
        return false;
    }
    else
    {
        LOG_DBG("Navigation thread memory slab allocated!");
    }

    if (k_mem_slab_alloc(&control_thread_mem, &control_thread_info, K_NO_WAIT)
     != 0)
    {
        LOG_ERR("Control thread memory slab failed to allocate!");
        return false;
    }
    else
    {
        LOG_DBG("Control thread memory slab allocated!");
    }

    if (k_mem_slab_alloc(&sensor_data_mem, &sensor_data, K_NO_WAIT) != 0)
    {
        LOG_ERR("Sensor data memory slab failed to allocate!");
        return false;
    }
    else
    {
        LOG_DBG("Sensor data memory slab allocated!");
    }

    if (k_mem_slab_alloc(&state_mem, &state, K_NO_WAIT) != 0)
    {
        LOG_ERR("State memory slab failed to allocate!");
        return false;
    }
    else
    {
        LOG_DBG("State memory slab allocated!");
    }

    if (k_mem_slab_alloc(&desired_state_mem, &desired_state, K_NO_WAIT) != 0)
    {
        LOG_ERR("Desired state memory slab failed to allocate!");
        return false;
    }
    else
    {
        LOG_DBG("Desired state memory slab allocated!");
    }

    if (k_mem_slab_alloc(&previous_state_mem, &previous_state, K_NO_WAIT) != 0)
    {
        LOG_ERR("Previous state memory slab failed to allocate!");
        return false;
    }
    else
    {
        LOG_DBG("Previous state memory slab allocated!");
    }

    if (k_mem_slab_alloc(&previous_desired_state_mem, &previous_desired_state,
        K_NO_WAIT) != 0)
    {
        LOG_ERR("Previous desired state memory slab failed to allocate!");
        return false;
    }
    else
    {
        LOG_DBG("Previous desired state memory slab allocated!");
    }

    // Initalize all the slab memory to zeros. Technically this isn't neccesary.
    memset(sensor_thread_info, 0, sizeof(struct Sensor_thread_info));
    memset(state_thread_info, 0, sizeof(struct Sensor_thread_info));
    memset(navigate_thread_info, 0, sizeof(struct Sensor_thread_info));
    memset(control_thread_info, 0, sizeof(struct Sensor_thread_info));

    memset(sensor_data, 0, sizeof(struct Sensor_data));
    memset(state, 0, sizeof(struct Auv_state));
    memset(desired_state, 0, sizeof(struct Auv_state));
    memset(previous_state, 0, sizeof(struct Auv_state));
    memset(previous_desired_state, 0, sizeof(struct Auv_state));

    return true;
}
