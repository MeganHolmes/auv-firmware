// Tests auv-state

// System includes
#include <stdlib.h>
#include <logging/log.h>

// Project includes
#include "config.h"
#include "state.h"
#include "test-state.h"

// functions constrained to this .c file
static bool test_init_state(void);
static bool test_estimate_state(void);
static bool test_output_state(void);

// Setup logging
LOG_MODULE_REGISTER(TEST_STATE, AUV_LOG_LEVEL);

/*******************************************************************************
 * @brief Tests all state functions
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_state(void)
{
    LOG_INF("Starting state tests");

    bool result = true;

    if(test_init_state())
    {
        LOG_DBG("Init state test passed");
    }
    else
    {
        LOG_ERR("Init state test failed");
        result = false;
    }

    if (test_estimate_state())
    {
        LOG_DBG("Estimate state test passed");
    }
    else
    {
        LOG_ERR("Estimate state test failed");
        result = false;
    }

    if (test_output_state())
    {
        LOG_DBG("Output state test passed");
    }
    else
    {
        LOG_ERR("Output state test failed");
        result = false;
    }

    return result;
}

/*******************************************************************************
 * @brief Tests the init_state function
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_init_state(void)
{
    struct Auv_state* state = init_auv_state();
    float init_val = 0;
    bool  result = true;

    if (state->pos_north != init_val)
    {
        LOG_ERR("Failed to initialize state.pos_north");
        result = false;
    }

    if (state->pos_east != init_val)
    {
        LOG_ERR("Failed to initialize state.pos_east");
        result = false;
    }

    if (state->pos_depth != init_val)
    {
        LOG_ERR("Failed to initialize state.pos_depth");
        result = false;
    }

    if (state->vel_north != init_val)
    {
        LOG_ERR("Failed to initialize state.vel_north");
        result = false;
    }

    if (state->vel_east != init_val)
    {
        LOG_ERR("Failed to initialize state.vel_east");
        result = false;
    }

    if (state->vel_depth != init_val)
    {
        LOG_ERR("Failed to initialize state.vel_depth");
        result = false;
    }

    if (state->acc_north != init_val)
    {
        LOG_ERR("Failed to initialize state.acc_north");
        result = false;
    }

    if (state->acc_east != init_val)
    {
        LOG_ERR("Failed to initialize state.acc_east");
        result = false;
    }

    if (state->acc_depth != init_val)
    {
        LOG_ERR("Failed to initialize state.acc_depth");
        result = false;
    }

    if (state->roll != init_val)
    {
        LOG_ERR("Failed to initialize state.roll");
        result = false;
    }

    if (state->pitch != init_val)
    {
        LOG_ERR("Failed to initialize state.pitch");
        result = false;
    }

    if (state->yaw != init_val)
    {
        LOG_ERR("Failed to initialize state.yaw");
        result = false;
    }

    if (state->vel_roll != init_val)
    {
        LOG_ERR("Failed to initialize state.vel_roll");
        result = false;
    }

    if (state->vel_pitch != init_val)
    {
        LOG_ERR("Failed to initialize state.vel_pitch");
        result = false;
    }

    if (state->vel_yaw != init_val)
    {
        LOG_ERR("Failed to initialize state.vel_yaw");
        result = false;
    }

    if (state->water_velocity != init_val)
    {
        LOG_ERR("Failed to initialize state.water_velocity");
        result = false;
    }

    if (state->watercurrent_north != init_val)
    {
        LOG_ERR("Failed to initialize state.watercurrent_north");
        result = false;
    }

    if (state->watercurrent_east != init_val)
    {
        LOG_ERR("Failed to initialize state.watercurrent_east");
        result = false;
    }

    if (state->ground_velocity != init_val)
    {
        LOG_ERR("Failed to initialize state.ground_velocity");
        result = false;
    }

    if (state->angle_of_attack != init_val)
    {
        LOG_ERR("Failed to initialize state.angle_of_attack");
        result = false;
    }

    if (state->sideslip_angle != init_val)
    {
        LOG_ERR("Failed to initialize state.sideslip_angle");
        result = false;
    }

    if (state->heading != init_val)
    {
        LOG_ERR("Failed to initialize state.heading");
        result = false;
    }

    free(state);

    return result;
}

/*******************************************************************************
 * @brief Tests the estimate_state function
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_estimate_state(void)
{
    LOG_WRN("Method test_estimate_state not yet implemented!");

    return false;
}

/*******************************************************************************
 * @brief Tests the output_state function
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_output_state(void)
{
    LOG_WRN("Method test_output_state not yet implemented!");

    return false;
}
