// Runs tests for all functions used in auv-main.c

// System includes
#include <stdlib.h>
#include <logging/log.h>

// Project includes
#include "constants.h"
#include "config.h"

#include "test-sensor.h"
#include "test-state.h"
#include "test-control.h"

// Setup logging
LOG_MODULE_REGISTER(TEST_MAIN, AUV_LOG_LEVEL);

void main(void)
{
    LOG_INF("Starting tests");

    if (test_state())
    {
        LOG_DBG("State tests passed");
    }
    else
    {
        LOG_ERR("State tests failed");
    }

    if (test_sensor())
    {
        LOG_DBG("Sensor tests passed");
    }
    else
    {
        LOG_ERR("Sensor tests failed");
    }

    if (test_control())
    {
        LOG_DBG("Control tests passed");
    }
    else
    {
        LOG_ERR("Control tests failed");
    }

    LOG_INF("Tests Complete");
    exit(0);
}
