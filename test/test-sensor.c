// Tests auv-sensor

// System includes
#include <assert.h>
#include <stdlib.h>
#include <logging/log.h>

//Project includes
#include "config.h"
#include "constants.h"
#include "sensor.h"
#include "test-sensor.h"

// Functions constrained to this .c file
bool test_retrieve_sensor_data(void);
bool test_check_for_possible_collision(void);
bool test_output_sensor_data(void);
void reset_ultrasounds(struct Sensor_data* data);

// Setup logging
LOG_MODULE_REGISTER(TEST_SENSOR, AUV_LOG_LEVEL);

/*******************************************************************************
 * @brief Tests all sensor functions
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_sensor(void)
{
    LOG_INF("Starting sensor tests");

    bool result = true;

    if(test_retrieve_sensor_data())
    {
        LOG_DBG("Retrieve sensor data test passed");
    }
    else
    {
        LOG_ERR("Retrieve sensor data test failed");
        result = false;
    }

    if (test_check_for_possible_collision())
    {
        LOG_DBG("Check for possible collision test passed");
    }
    else
    {
        LOG_ERR("Check for possible collision test failed");
        result = false;
    }

    if (test_output_sensor_data())
    {
        LOG_DBG("Output sensor data test passed");
    }
    else
    {
        LOG_ERR("Output sensor data test failed");
        result = false;
    }

    return result;
}

bool test_retrieve_sensor_data(void)
{
    LOG_WRN("Method test_retrieve_sensor_data not yet implemented!");
    return false;
}

/*******************************************************************************
 * @brief Tests the check_for_possible_collision function
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_check_for_possible_collision(void)
{
    assert(MINIMUM_OBSTACLE_CLEARANCE != 0);

    struct Sensor_data* data = retrieve_sensor_data();
    reset_ultrasounds(data);

    bool result = true;

    if (check_for_possible_collision(data)) // Should return false
    {
        LOG_ERR("all ultrasounds > minimum returned true!");
        result = false;
    }

    data->forward_ultrasound_left = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("forward_left < minimum returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->forward_ultrasound_right = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("forward_right < minimum returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->left_ultrasound = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("left < minimum returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->right_ultrasound = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("right < minimum returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->bottom_ultrasound = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("bottom < minimum returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->reverse_ultrasound = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("reverse < minimum returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->forward_ultrasound_left = 0;
    data->forward_ultrasound_right = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("forward_left and forward_right returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->reverse_ultrasound = 0;
    data->forward_ultrasound_right = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("forward_right and reverse returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->right_ultrasound = 0;
    data->left_ultrasound = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("right and left returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->left_ultrasound = 0;
    data->bottom_ultrasound = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("left and bottom returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->left_ultrasound = 0;
    data->forward_ultrasound_left = 0;
    data->forward_ultrasound_right = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("forward_left,forward_right and left returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->left_ultrasound = 0;
    data->forward_ultrasound_left = 0;
    data->forward_ultrasound_right = 0;
    data->bottom_ultrasound = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("forward_left, forward_right, left, and bottom returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->left_ultrasound = 0;
    data->forward_ultrasound_left = 0;
    data->forward_ultrasound_right = 0;
    data->bottom_ultrasound = 0;
    data->right_ultrasound = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("forward_left, forward_right, left, bottom and right returned false!");
        result = false;
    }

    reset_ultrasounds(data);
    data->left_ultrasound = 0;
    data->forward_ultrasound_left = 0;
    data->forward_ultrasound_right = 0;
    data->bottom_ultrasound = 0;
    data->right_ultrasound = 0;
    data->reverse_ultrasound = 0;

    if (!check_for_possible_collision(data)) // Should return true
    {
        LOG_ERR("all ultrasounds < minimum clearance returned false!");
        result = false;
    }

    free(data);

    return result;
}

/*******************************************************************************
 * @brief Tests the output_sensor_data function
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_output_sensor_data(void)
{
    LOG_WRN("Method test_output_sensor_data not yet implemented!");
    return false;
}

/*******************************************************************************
 * @brief Resets the ultrasound values to a value greater than the minimum
 *        clearance
 *
 * @param data pointer to Sensor_data struct to be modified
 ******************************************************************************/
void reset_ultrasounds(struct Sensor_data* data)
{
    data->forward_ultrasound_left  = MINIMUM_OBSTACLE_CLEARANCE+1;
    data->forward_ultrasound_right = MINIMUM_OBSTACLE_CLEARANCE+1;
    data->left_ultrasound          = MINIMUM_OBSTACLE_CLEARANCE+1;
    data->right_ultrasound         = MINIMUM_OBSTACLE_CLEARANCE+1;
    data->bottom_ultrasound        = MINIMUM_OBSTACLE_CLEARANCE+1;
    data->reverse_ultrasound       = MINIMUM_OBSTACLE_CLEARANCE+1;
}
