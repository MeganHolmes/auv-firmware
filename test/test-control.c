// Tests auv-control

// System includes
#include <stdlib.h>
#include <logging/log.h>

// Project includes
#include "config.h"
#include "constants.h"
#include "control.h"
#include "test-control.h"

// Functions constrained to this .c file
bool test_avoid_collision(void);
bool test_translate_desired_state_to_commands(void);
bool test_execute_commands(void);
bool test_output_commands(void);

// Setup logging
LOG_MODULE_REGISTER(TEST_CONTROL, AUV_LOG_LEVEL);

/*******************************************************************************
 * @brief Tests all control functions
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_control(void)
{
    LOG_INF("Starting control tests");

    bool result = true;

    if(test_avoid_collision())
    {
        LOG_DBG("Avoid collision test passed");
    }
    else
    {
        LOG_ERR("Avoid collision test failed");
        result = false;
    }

    if(test_translate_desired_state_to_commands())
    {
        LOG_DBG("Translate state to commands test passed");
    }
    else
    {
        LOG_ERR("Translate state to commands test failed");
        result = false;
    }

    if(test_execute_commands())
    {
        LOG_DBG("Execute Commands test passed");
    }
    else
    {
        LOG_ERR("Execute Commands test failed");
        result = false;
    }

    if(test_output_commands())
    {
        LOG_DBG("Output commands test passed");
    }
    else
    {
        LOG_ERR("Output commands test failed");
        result = false;
    }

    return result;
}

/*******************************************************************************
 * @brief Tests the avoid_collision function
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_avoid_collision(void)
{
    LOG_WRN("Method test_avoid_collision not yet implemented!");
    return false;
}

/*******************************************************************************
 * @brief Tests the translate_desired_state_to_commands function
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_translate_desired_state_to_commands(void)
{
    LOG_WRN("Method test_translate_desired_state_to_commands not yet implemented!");
    return false;
}

/*******************************************************************************
 * @brief Tests the execute_commands function
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_execute_commands(void)
{
    LOG_WRN("Method execute_commands not yet implemented!");
    return false;
}

/*******************************************************************************
 * @brief Tests the output_commands function
 *
 * @return True if all tests pass, false otherwise
 ******************************************************************************/
bool test_output_commands(void)
{
    LOG_WRN("Method output_commands not yet implemented!");
    return false;
}
