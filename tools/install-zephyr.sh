# This script installs all the stuff required for Zephyr development
#
echo 'Do not run this as sudo, it breaks west. Make sure you have at least 10GB of free space. This can take up an hour. If you want to cancel this script you have 10 seconds to hit Ctrl+C'

sleep 10

# Update system
sudo apt update
sudo apt upgrade -y

# Install dependencies
sudo apt install --no-install-recommends git openssl ninja-build gperf \
  ccache libssl-dev dfu-util device-tree-compiler wget \
  python3-dev python3-pip python3-setuptools python3-tk python3-wheel \
  xz-utils file make gcc gcc-multilib g++-multilib libsdl2-dev -y

pip3 install pyelftools

# Update cmake (this is needed for Ubuntu)
sudo apt remove cmake -y
cd ~
wget https://github.com/Kitware/CMake/releases/download/v3.22.0/cmake-3.22.0.tar.gz
tar -xvf cmake-3.22.0.tar.gz
rm cmake-3.22.0.tar.gz
cd ~/cmake-3.22.0
./bootstrap
make -j$(nproc)
sudo make install
cd ~
rm -rf cmake-3.22.0

# Make sure west is on the path.
export PATH=~/.local/bin:"$PATH" >> ~/.bashrc
source ~/.bashrc

# Install west
pip3 install --user -U west

# Get Zephyr source code
west init ~/zephyrproject
cd ~/zephyrproject
west update

# Export Cmake package
west zephyr-export

# Install additional dependencies
pip3 install --user -r ~/zephyrproject/zephyr/scripts/requirements.txt

# Download Zephyr SDK
cd ~
wget https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v0.13.2/zephyr-sdk-0.13.2-linux-x86_64-setup.run

# Run Zephyr Installer
chmod +x zephyr-sdk-0.13.2-linux-x86_64-setup.run
./zephyr-sdk-0.13.2-linux-x86_64-setup.run -- -d ~/zephyr-sdk-0.13.2

# Install udev to allow flashing boards
sudo cp ~/zephyr-sdk-0.13.2/sysroots/x86_64-pokysdk-linux/usr/share/openocd/contrib/60-openocd.rules /etc/udev/rules.d
sudo udevadm control --reload
rm zephyr-sdk-0.13.2-linux-x86_64-setup.run

# Make sure Zephyr is on the path
source ~/zephyrproject/zephyr/zephyr-env.sh >> ~/.bashrc
source ~/.bashrc

# Build and run hello world example to check everything is working
cd ~/zephyrproject/zephyr/samples/hello_world
west build -b native_posix
echo "Attempting to run hello world example, if it succeeds you should see 'Hello World' printed to the screen. Use Ctrl+C to exit."
west build -t run
